import React from 'react'
import './App.css'
import {LoginForm} from './components/main-options/Auth/LoginForm'
import {ajaxUrl, inputFormTypes, keys, messages, outputLineTypes} from './constants'
import {
  addAccessGranted,
  addExecutable,
  addGuestHost,
  addInput,
  addNoCommandFound,
  addSuggestions,
  addWrongPassword,
  concatInput,
  getLoginAction,
  getSuggestions,
  initialOutput,
  inputIsValid,
  isClear,
  isPassword,
} from './utils'
import {setTexts} from "./storage/customTexts"

export default class App extends React.Component {
  defaultState = {
    input: "",
    inputType: inputFormTypes.default,
    loginCommand: null,
    executedCommands: [],
    mSyndicatePassword: '',
    eRewardsPassword: '',
    error: '',
    loaded: false,
  }

  constructor(props){
    super(props);
    this.state = this.defaultState;
    this.defaultInputRef = React.createRef();
    this.loginInputRef = React.createRef();
    this.dynamicRef = React.createRef();
    this.escFunction = this.escFunction.bind(this);
  }

  extractStateForExecutableCommands() {
    const { eRewardsPassword, mSyndicatePassword } = this.state;
    return { eRewardsPassword, mSyndicatePassword };
  }

  getRefInput = () => {
    const inputType = this.state.inputType;
    if (inputType === inputFormTypes.default) {
      return this.defaultInputRef;
    } else if (inputType === inputFormTypes.loginForm) {
      return this.loginInputRef;
    } else if (inputType === inputFormTypes.empty) {
      return this.dynamicRef;
    }

    return null;
  }
  
  // Handle key press (Alphanumeric)
  handleKeyEvent = (key) => {
    var element = document.getElementsByClassName("console");
    element.scrollTop = element.scrollHeight;

    let { input, output } = this.state;
    input = input.concat(key);
    output = concatInput(key, output);
    this.setState({
      input,
      output
    });
  }

  // Handle control key events
  handleControlKeyEvent = (key, e) => {
    switch(key) {
      case keys.tab:
        e.preventDefault();
        this.handleTabEvent();
        break;  
      case keys.up:
        e.preventDefault();
        this.handleUpEvent();
        break;
      case keys.down:
        e.preventDefault();
        this.handleDownEvent();
        break;
      default: return;
    }
  }

  handleInput = (artificialCommand) => {
    let { input, output, executedCommands, currentCommand } = this.state;
    let associatedCommand = artificialCommand ?? inputIsValid(input);
    let inputType = artificialCommand ? inputFormTypes.empty : inputFormTypes.default;
    let loginCommand = null;

    output = addInput(output, input);
    if (associatedCommand) {
      executedCommands.push(input);
      if (isPassword(input)) {
        addExecutable(associatedCommand, output, this.extractStateForExecutableCommands());
        inputType = inputFormTypes.loginForm;
        loginCommand = associatedCommand;
      }
      else if (isClear(input)) {
        output = addGuestHost([]);
      }
      else {
        addExecutable(associatedCommand, output, this.extractStateForExecutableCommands());
        if (inputType !== inputFormTypes.empty) {
          addGuestHost(output);
        }
      }
    }
    else if(input === "" || input === " ") {
      addGuestHost(output);
    }
    else {
      addNoCommandFound(output, input);
      addGuestHost(output);
    }

    this.setState({
      input: "",
      inputType,
      loginCommand,
      output,
      executedCommands,
      currentCommand,
    });
  }

  // Handle Enter control key
  handleEnterKeyEvent = (e) => {
    e.preventDefault();
    this.handleInput();
  }

  handleTabEvent = () => {
    let { input, output } = this.state;
    let suggestions = getSuggestions(input);
    if(input === "" || suggestions.length === 0) return;
    else if(suggestions.length === 1) {
      input = suggestions[0];
    } else {
      addInput(output, input)
      addSuggestions(input, output);
      addGuestHost(output);
    }
    this.setState({
      input: suggestions[0],
      output
    });
  }

  handleUpEvent = () => {
    let { input, executedCommands, currentCommand } = this.state;
    if(executedCommands.length === 0) return;
    if(currentCommand <= 0) currentCommand = executedCommands.length - 1;
    else currentCommand -= 1;
    input = executedCommands[currentCommand];
    this.setState({
      input,
      currentCommand
    });
  }

  handleDownEvent = () => {
    let { input, executedCommands, currentCommand } = this.state;
    if(executedCommands.length === 0) return;
    if(currentCommand >= executedCommands.length - 1) currentCommand = 0;
    else currentCommand += 1;
    input = executedCommands[currentCommand];
    this.setState({
      input,
      currentCommand
    });
  }

  handleCursorFocus = () => {
    // Scroll to input and focus it
    const refEl = this.getRefInput().current;
    if (!refEl) {
      return;
    }

    refEl.scrollIntoView();
    refEl.focus();
    window.onclick = () => {
      refEl.focus();
    };
  }

  handleWindowClick = () => {
    window.addEventListener('keydown', e => {
      this.handleControlKeyEvent(e.key, e);
    });
  }
  escFunction(event){
    if (event.key === "Escape") {
      this.handleTakeBackControl();
      this.setState({
        output: initialOutput(),
      }, () => this.handleCursorFocus());
    }
  }
  componentDidUpdate = () => {
    this.handleCursorFocus();
  }

  componentDidMount = () => {
    this.loadCustomTexts();
    this.handleCursorFocus();
    this.handleWindowClick();
    document.addEventListener("keydown", this.escFunction, false);
  }
  componentWillUnmount(){
    document.removeEventListener("keydown", this.escFunction, false);
  }

  loadCustomTexts = async () => {
    const requestInfo = {
      method: 'POST',
      url: ajaxUrl,
      withCredentials: true,
      mode: 'no-cors',
      headers: {
        'Content-Type': 'x-www-form-urlencoded;charset=utf-8',
        'Access-Control-Allow-Origin': '*'
      },
      body: new URLSearchParams({
        action: 'get_texts'
      })
    }

    const response = await fetch(ajaxUrl, requestInfo)
    const dataReceived = await response.json();

    if (response.ok === true) setTexts(dataReceived.data)

    else {
      this.setState({ error: response.error });
      console.log(response.error)
    }

    this.setState({
      loaded: true,
      output: initialOutput(),
    });
  }

  handleInputChange = e => {
    let input = e.target.value;
    this.setState({
      input
    })
  }

  handleLogging = async (isLoggedIn, correctPassword) => {
    let {output, loginCommand, mSyndicatePassword, eRewardsPassword} = this.state;

    await isLoggedIn
        ? addAccessGranted(output)
        : addWrongPassword(output);

    

    const password = isLoggedIn ? correctPassword : '';
    let artificialCommand = null;

    if (loginCommand === 'mSyndicate' && isLoggedIn) {
      mSyndicatePassword = password;
      artificialCommand = 'mSyndicatePage';
    }
    
    else if (loginCommand === 'eRewards' && isLoggedIn) {
      eRewardsPassword = password;
      artificialCommand = 'eRewardsPage'
    }

    this.setState({
      mSyndicatePassword,
      eRewardsPassword,
      output
    }, () => this.handleInput(artificialCommand));
  }

  handleTakeBackControl = async () => {
    this.handleInput();
  }

  render() {
    const { input, inputType, loginCommand, output, loaded, error } = this.state;
    let inputIsEmpty = false;
    return (
      !loaded ||
        error && <p>{error}</p> ||
      <div className="console" id="console">
        <div className='console-wrapper'>
          <div>
            { /* Render output here */ }
            { output.map((element, index) => {
              switch(element.type) {
                case outputLineTypes.executable:
                case outputLineTypes.noCommandFound:
                case outputLineTypes.wrongPassword:
                case outputLineTypes.accessGranted:
                  return <element.component props={element.props} key={index} innerRef={this.dynamicRef} takeBackControl={this.handleTakeBackControl}/>;
                case outputLineTypes.input:
                    if(element.props.text === "" || element.props.text === messages.welcome()) inputIsEmpty = true;
                    return <element.component props={element.props} key={index} />;
                case outputLineTypes.suggestions:
                    return <element.component suggestions={element.suggestions} key={index} />;
                case outputLineTypes.guestHost:
                case outputLineTypes.enterPassword:
                    return inputIsEmpty ? (
                      <span key={index}>
                        <br />
                        <element.component props={element.props} />
                      </span>
                      ) : (
                        <element.component key={index} />
                      );
                default: return null;
              }
            })}
            {(() => {
              switch (inputType) {
                case inputFormTypes.empty:
                  return <></>;
                case inputFormTypes.loginForm:
                  return <LoginForm ref={this.loginInputRef} logged={this.handleLogging} loginAction={getLoginAction(loginCommand)} />;
                case inputFormTypes.default:
                default:
                  return <form onSubmit={this.handleEnterKeyEvent}>
                    <input type="text" value={input} placeholder={""} ref={this.defaultInputRef} autoFocus onChange={this.handleInputChange} />
                  </form>;
              }
            })()}
          </div>
        </div>
        
        <div>
          <div className='escape-key-information'>
            Press <span>ESC</span> to return to the main menu and/or 
            type <span>'help'</span> to see a list of all available commands.
          </div>
        </div>
      </div>
    );
  }
}
