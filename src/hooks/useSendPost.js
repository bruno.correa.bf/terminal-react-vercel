import { useEffect, useState } from 'react'

export const useSendPost = (url, payload) => {
  const [data, setData] = useState(null)
  const [error, setError] = useState("")
  const [loaded, setLoaded] = useState(false)
  useEffect(() => {
    (async () => {
      try {
        const response = await fetch(
          url,
          payload
        );
        setData(response.data)
      }
      
      catch (error) {
        setError(error.message)
      }
      
      finally {
        setLoaded(true)
      }
    })()
  }, [])
  
  return { data, error, loaded }
};