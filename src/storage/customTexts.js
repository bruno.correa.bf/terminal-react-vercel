let customTexts = {};

export function setTexts(texts) {
  customTexts = texts;
}

export function getTexts() {
  return customTexts;
}