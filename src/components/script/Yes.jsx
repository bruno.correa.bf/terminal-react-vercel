import React from 'react'
import { Typing } from 'react-typing'
import { yesOptions } from "../../constants";

export default function Yes() {
  return (
    <div className="yes">
      <Typing keyDelay={30}>
        YES - Please continue with one of the following commands:
      </Typing >

      <table>
        <tbody>
          { yesOptions.map((cmd, index) => {
            return (
              <tr key={index}>
                <td style={{ fontWeight: "bold" }}>
                  { cmd.aliases.join(",") }
                </td>
                <span className='help-divider'>
                  -
                </span>
                <td>
                  { cmd.description }
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  )
}