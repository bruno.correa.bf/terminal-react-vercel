import React from "react";

const styleDiv = {
    color: "#e958ff"
}

export default function WrongPassword({props}) {
    const { text } = props;
    return (
        <div className="wrongPassword" style={styleDiv}>
            { text }
        </div>
    );
}