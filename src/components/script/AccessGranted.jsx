import React from "react";

const styleDiv = {
    color: "#a489ff"
}

export default function AccessGranted({props}) {
    const { text } = props;
    return (
        <div style={styleDiv}>
            { text }
        </div>
    );
}