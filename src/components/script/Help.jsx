import React from 'react';
import {commands} from "../../constants";

export default function Help(){
    return (
        <div>
            This is a list of all available commands:
            <table>
                <tbody>
                    { Object.entries(commands).map(([key,cmd], index) => {
                        if (cmd.hidden) {
                            return;
                        }
                        return (
                            <tr key={index}>
                            <td style={{ fontWeight: "bold" }}>
                                { cmd.aliases.join(",") }
                            </td>
                            <span className='help-divider'>
                              -
                            </span>
                            <td>
                                { cmd.description }
                            </td>
                        </tr>
                        )
                    })}
                </tbody>
            </table>
            <br />
            Press 'tab' for auto-completion
        </div>
    );
}