import React from 'react';
import { Typing } from 'react-typing'

export default class NewWelcome extends React.Component {
    render() {
        return (
            <div className="new-welcome">
              <strong>Muunifi </strong>
              {' '}
                <Typing keyDelay={30}>
                  The future of investing isn't coming... It's here.
                  <br />
                </Typing >
            </div>
        )
    }
}