import React from 'react'
import { Typing } from 'react-typing'

export default function No() {
  setTimeout(function() {
    window.location.href = 'https://cnbc.com';
  }, 1250);

  return (
    <div className="no">
      <Typing keyDelay={30}>
        NO - Have fun staying poor.
      </Typing >
    </div>
  )
}