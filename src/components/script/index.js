import React from "react";
import Help from "./Help";
import Reset from "./Reset";
import Yes from "./Yes";
import No from "./No";

//main menu
import { Team } from './../main-options/Team'
import { BusinessSummary } from './../main-options/BusinessSummary'
import { MSyndicateApplication } from './../main-options/MSyndicateApplication'
import { MSyndicatePageApplication } from '../main-options/MSyndicatePageApplication'
import { EarlyRewardsApplication } from '../main-options/EarlyRewardsApplication'
import { EarlyRewardsPageApplication } from '../main-options/EarlyRewardsPageApplication'
import { CitadelMembersList } from '../main-options/CitadelMembersList'

function renderComponent (command, state, innerRef, takeBackControl) {
    switch(command){
        case 'help':
            return <Help />;
        case 'reset':
            return <Reset />;
        case 'yes':
            return <Yes />;
        case 'no':
            return <No />;
        case 'mSyndicate':
          return <MSyndicateApplication />;
        case 'mSyndicatePage':
          return <MSyndicatePageApplication password={ state.mSyndicatePassword } innerRef={ innerRef } takeBackControl={ takeBackControl }/>;
        case 'Team':
          return <Team />;
        case 'bSummary':
          return <BusinessSummary />;
        case 'citadelList':
          return <CitadelMembersList />;
        case 'eRewards':
          return <EarlyRewardsApplication />;
        case 'eRewardsPage':
          return <EarlyRewardsPageApplication password={ state.eRewardsPassword } innerRef={ innerRef} takeBackControl={ takeBackControl }/>
        default: return null;
    }
}

export function Script({props, innerRef, takeBackControl}) {
    const { command, state } = props;
    return (
        <div className="script">
            {
                renderComponent(command, state, innerRef, takeBackControl)
            }
        </div> 
    );
}

export function YesScript({props, innerRef}) {
  const { yesOption, state } = props;
  return (
      <div className="script">
          {
              renderComponent(yesOption, state, innerRef)
          }
      </div> 
  );
}