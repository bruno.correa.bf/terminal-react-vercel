import React, {useEffect, useState} from 'react'

import {ajaxUrl} from '../../constants'
import {LoadingData} from "./Auth/LoadingData";

const styleError = {
  color: "#e958ff;"
}

export function EarlyRewardsPageApplication({ password, takeBackControl }) {
  const [data, setData] = useState([])
  const [error, setError] = useState("")
  const [loading, setLoading] = useState(false)

  const requestInfo = {
    method: 'POST',
    url: ajaxUrl,
    withCredentials: true,
    mode: 'no-cors',
    headers: {
      'Content-Type': 'x-www-form-urlencoded;charset=utf-8',
      'Access-Control-Allow-Origin': '*'
    },
    body: new URLSearchParams({
      action: 'form_early_rewards_password',
      password: password
    })
  }

  const sendForm = async () => {
    setLoading(true)
    const response = await fetch(ajaxUrl, requestInfo)
    const receivedData = await response.json();

    if (response.ok === true) setData(receivedData.data)

    else {
      console.log(receivedData.data)
      setError(receivedData.data)
    }

    setLoading(false)
    takeBackControl()
  }

  useEffect(() => {
    sendForm()
  }, [])

  return (
    <>
      <div className="early-rewards-page-application">
        <br />
        {loading && <LoadingData />}
        {error && <p style={styleError}>error</p>}
        {
          <span
            dangerouslySetInnerHTML={{ __html: data.text }}
          />
        }
      </div>
     </>
  )
}