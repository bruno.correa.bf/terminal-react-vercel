import React from 'react'
import {getTexts} from "../../storage/customTexts";

export function Team() {
  const members = getTexts().members;
  return (
    <>
      <div className="team-members-page">
        <strong>Team members:</strong>
        <br />
        <br />
        {members.map((d) =>
            <li className="member-item" key={d.name}>
              <a className='member-link' key={d.name} target='_blank' href={d.link}>
                <span>{d.name}</span> - {d.bio}
              </a>
            </li>
        )}
      </div>
     </>
  )
}