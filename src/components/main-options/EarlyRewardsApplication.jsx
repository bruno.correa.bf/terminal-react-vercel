import { getTexts } from '../../storage/customTexts'

export function EarlyRewardsApplication() {
  const texts = getTexts()

  return (
    <>
      <div className="early-rewards-application">
          <span
            dangerouslySetInnerHTML={{ __html: texts.ear_summary_description }}
          />
      </div>
     </>
  )
}