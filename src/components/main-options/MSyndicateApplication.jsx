import React from 'react';
import {getTexts} from "../../storage/customTexts";

export function MSyndicateApplication() {
  const texts = getTexts()
  return (
    <>
      <div className="msyndicate-application">
        <span
          dangerouslySetInnerHTML={{ __html: texts.msyndicate_bio }}
        />
      </div>
     </>
  )
}