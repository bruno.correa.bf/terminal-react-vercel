import React from 'react'
import {YoutubeEmbedVideoPlayer} from './VideoPlayer'
import {getTexts} from "../../storage/customTexts";

export function BusinessSummary() {
  const texts = getTexts();
  return (
    <>
      <div className="video-player">
        <YoutubeEmbedVideoPlayer embedId={ texts.bs_video }/>
      </div>
      <div className="business-summary-description">
        <span
          dangerouslySetInnerHTML={{ __html: texts.bs_description }}
        />
      </div>
    </>
  )
}