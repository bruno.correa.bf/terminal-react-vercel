import { getTexts } from '../../storage/customTexts'

export function CitadelMembersList() {
  const texts = getTexts();
  return (
    <>
      <div className="citadel-members-list-description">
        <span
          dangerouslySetInnerHTML={{ __html: texts.citadel_members_list }}
        >
        </span>
      </div>
    </>
  )
}