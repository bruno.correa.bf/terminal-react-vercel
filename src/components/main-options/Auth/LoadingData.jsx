import React from 'react'

export const LoadingData = () => {
  return (
    <>
    <div className='dots-wrapper'>
      <div className="col-3">
        <div className="snippet">
          <div className="stage">
            <div className="dot-flashing"></div>
          </div>
        </div>
      </div>
    </div>
    </>
  )
}