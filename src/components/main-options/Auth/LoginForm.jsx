import React, { useState } from 'react'

import EnterPassword from '../../enter-password'
import { ajaxUrl, messages } from '../../../constants'
import { LoadingData } from './LoadingData'

export const LoginForm = React.forwardRef((props, ref) => {
  const currentAction = props.loginAction

  const [password, setPassword] = useState('')
  const [error, setError] = useState('')
  const [mydata, setMydata] = useState(null)
  const [loading, setLoading] = useState(false)
  const [counter, setCounter] = useState(1)

  const incrementCounter = () => {
    setCounter(counter + 1)
  }

  const handleFormSubmit = async (e) => {
    e.preventDefault()
    setLoading(true)

    const requestInfo = {
      method: 'POST',
      url: ajaxUrl,
      withCredentials: true,
      mode: 'no-cors',
      headers: {
        'Content-Type': 'x-www-form-urlencoded;charset=utf-8',
        'Access-Control-Allow-Origin': '*'
      },
      body: new URLSearchParams({
        password: password,
        action: currentAction
      })
    }

    await fetch(ajaxUrl, requestInfo)
      .then(response => {
        if (response.ok === true) return props.logged(true, password)
        throw new Error('wrong password')
      })

      .catch(error => {
        setError(error)
        incrementCounter()
        setTimeout(() => {
          if (counter === 3) {
            props.logged(false)
          }
        }, 0)
      })

      .finally(() => setLoading(false))
  }

  return (
    <>
    <label>
      <EnterPassword />
      <form onSubmit={ handleFormSubmit }>
        <input
            ref={ ref }
            autoFocus
            type="password"
            value={ password }
            onChange={(e) => setPassword(e.target.value)}
        />
      </form>
      {error && <p className='password-try-again'>{ messages.tryAgainPassword }</p>}
    </label>

    { !loading ? mydata : <LoadingData /> }
    </>
  )
})
