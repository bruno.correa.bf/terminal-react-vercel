import { useState } from 'react'
import { ajaxUrl } from '../../constants'
import { Typing } from 'react-typing'
import { LoadingData } from './Auth/LoadingData'

const styleLabel = {
  display: "block"
}
const styleError = {
  color: "#e958ff"
}
const styleSuccess = {
  color: "#a489ff"
}

export function MSyndicatePageApplication({ password, takeBackControl }) {
  const handleChange = e => {
    const { name, value } = e.target
    setValues(prevState => ({
      ...prevState,
      [name]: value
    }))
  }

  const focusNextOnEnter = (event) => {
    if (event.key.toLowerCase() === "enter") {
      setDisplayedInputs(prevState => [...prevState, index + 1])
      const form = event.target.form
      const index = [...form].indexOf(event.target)
      setTimeout(() => form.elements[index + 1].focus(), 0)
      event.preventDefault()
    }
  };

  const isDisabled = () => {
    return loading || data
  }
  
  const triggerSubmit = (event) => {
    if (event.key.toLowerCase() === "enter") {
      event.target.form.dispatchEvent(new Event("submit", { bubbles: true, cancelable: true }))
    }
  }

  const [displayedInputs, setDisplayedInputs] = useState([0])
  const [data, setData] = useState(null)
  const [error, setError] = useState("")
  const [loading, setLoading] = useState(false)
  const [values, setValues] = useState({
    name: '',
    twitterhandle: '',
    telegram: '',
    otherusernameandsocials: '',
    email: '',
    whoinvitedyou: '',
  })
  const inputs = [
    {
      name: 'name',
      type: 'text',
      typing: 'Name: ',
      onKeyDown: focusNextOnEnter,
      isAutoFocus: true
    },
    {
      name: 'twitterhandle',
      type: 'text',
      typing: 'Twitter Handle: ',
      onKeyDown: focusNextOnEnter,
      isAutoFocus: false
    },
    {
      name: 'telegram',
      type: 'text',
      typing: 'Telegram: ',
      onKeyDown: focusNextOnEnter,
      isAutoFocus: false
    },
    {
      name: 'otherusernameandsocials',
      type: 'text',
      typing: 'Other Usernames and socials (i.e. @muunifi - Instagram): ',
      onKeyDown: focusNextOnEnter,
      isAutoFocus: false
    },
    {
      name: 'email',
      type: 'email',
      typing: 'Email: ',
      onKeyDown: focusNextOnEnter,
      isAutoFocus: false
    },
    {
      name: 'whoinvitedyou',
      type: 'text',
      typing: 'Who invited you: ',
      onKeyDown: triggerSubmit,
      isAutoFocus: false
    }
  ]

  const requestInfo = {
    method: 'POST',
    url: ajaxUrl,
    withCredentials: true,
    mode: 'no-cors',
    headers: {
      'Content-Type': 'x-www-form-urlencoded;charset=utf-8',
      'Access-Control-Allow-Origin': '*'
    },
    body: new URLSearchParams({
      action: 'send_form_msyndicate_password',
      password: password,
      ...values
    })
  }

  const sendForm = async (event) => {
    event.preventDefault()

    setLoading(true)
    const response = await fetch(ajaxUrl, requestInfo)
    const dataReceived = await response.json()

    if (response.ok === true) {
      setData(dataReceived)
      setError('')
      takeBackControl()
    } else {
      setError(dataReceived.data)
      setTimeout(() => event.target.elements[0].focus(), 0)
    }
    setLoading(false)
  }

  return (
    <>
      <div className="msyndicate-page-application">
        <form onSubmit={sendForm}>
          {
            displayedInputs.map((input, index) => {
              const { name } = inputs[index].name
              return (
              <label key={ index } style={ styleLabel }>
                <Typing>{ inputs[index].typing }</Typing>
                <input
                  onKeyDown={ inputs[index].onKeyDown }
                  type={ inputs[index].type }
                  value={ values[name] }
                  name={ inputs[index].name }
                  onChange={ handleChange }
                  autoFocus={ inputs[index].isAutoFocus }
                  disabled={ isDisabled() }
                  required
                />
              </label>
              )
            })
          }

          {loading && <LoadingData />}
          {error && error.message &&
            <p style={ styleError }>{ error.message }</p> ||
            <p style={ styleError }>{ error }</p>
          }
          {error && error.details && Object.entries(error.details).map(([key,message]) => (
              <p key={ key } style={ styleError }>{ message }</p>
            ))
          }
          {data && data.success &&
            <p className='data-success' style={ styleSuccess } dangerouslySetInnerHTML={{ __html: data.data.message }} ></p>
          }
        </form>
      </div>
     </>
  )
}