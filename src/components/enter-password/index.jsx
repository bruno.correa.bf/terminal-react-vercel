import React from "react";
import {Typing} from "react-typing";

// Displayed in the beginning of each line
// E.g: Enter password: <password>
export default function () {
    return (
        <Typing>Enter password: </Typing>
    )
}
