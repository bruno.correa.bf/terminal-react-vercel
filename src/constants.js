import {getTexts} from './storage/customTexts'

export const guest = "guest";

export const separator = "@";

export const host = "Notebook";

export const colon = ":";

export const home = "~";

export const dollar = "$";

export const ajaxUrl = `${process.env.REACT_APP_SERVER_BASE_URL}/wp-admin/admin-ajax.php`

export const inputFormTypes = {
    "default": "default",
    "loginForm": "loginForm",
    "empty": "empty",
}

export const outputLineTypes = {
    "guestHost": "guestHost",
    "enterPassword": "enterPassword",
    "input": "input",
    "executable": "executable",
    "noCommandFound": "noCommandFound",
    "wrongPassword": "wrongPassword",
    "accessGranted": "accessGranted",
    "suggestions": "suggestions"
}

export const messages = {
    "help": "For help, type help or -h",
    "noCommandFound": "no command was found",
    "tryAgainPassword": "Password incorrect - Please try again.",
    "wrongPassword": "access denied",
    "accessGranted": "access granted",
    "welcome": () => getTexts().hp_question_text
};

export const commands = {
    clear: {
        "name": "clear",
        "aliases": ["clear"],
        "description": "Clear the output"
    },
    help: {
        "name": "help",
        "aliases": ["help"],
        "description": "List available commands"
    },
    reset: {
        "name": "reset",
        "aliases": ["reset"],
        "description": "reset terminal"
    },
    yes: {
        "name": "yes",
        "aliases": ["Y", "y"],
        "description": "Please start selecting this or no"
    },
    no: {
        "name": "no",
        "aliases": ["N", "n"],
        "description": "Please start selecting this or yes"
    },
    mSyndicate: {
        "name": "mSyndicate application",
        "aliases": ["C", "c"],
        "description": "Application to the Citadel of Content Creators",
        "loginAction": "send_check_msyndicate_password"
    },
    mSyndicatePage: {
        "aliases": ["mSyndicatePage"],
        "hidden": true
    },
    citadelList: {
      "name": "Citadel members list",
      "aliases": ["L", "l"],
      "description": "List of Accepted Citadel Members"
    },
    Team: {
        "name": "Team",
        "aliases": ["T", "t"],
        "description": "Team"
    },
    bSummary: {
        "name": "Business summary",
        "aliases": ["S", "s"],
        "description": "Business summary"
    },
    eRewards: {
        "name": "Early adopter rewards",
        "aliases": ["R", "r"],
        "description": "Early adopter rewards",
        "loginAction": "send_check_early_rewards_password",
    },
    eRewardsPage: {
        "aliases": ["eRewardsPage"],
        "hidden": true
    }
}

export const passwordRequiredCommands = [
    "mSyndicate",
    "eRewards",
];

export const yesOptions = [
    {
      "name": "Citadel",
      "aliases": ["C"],
      "description": "Application to the Citadel of Content Creators"
    },
    {
      "name": "Citadel members list",
      "aliases": ["L"],
      "description": "List of Accepted Citadel Members"
    },
    {
      "name": "Team",
      "aliases": ["T"],
      "description": "Team"
    },
    {
      "name": "Business summary",
      "aliases": ["S"],
      "description": "Business summary"
    },
    {
      "name": "Early adopter rewards",
      "aliases": ["R"],
      "description": "Early adopter rewards"
    }
];

// All executables with html
export const executables = [
    "help"
];

// Keyboard keys
export const keys = {
    "enter": "Enter",
    "esc": "Escape",
    "tab": "Tab",
    "up": "ArrowUp",
    "down": "ArrowDown"
}