import {commands, messages, outputLineTypes, passwordRequiredCommands} from "./constants";
import GuestHost from "./components/guest-host";
import EnterPassword from "./components/enter-password";
import Input from "./components/Input";
import {Script, YesScript} from "./components/script";
import NoCommandFound from "./components/script/NoCommandFound";
import Suggestions from "./components/script/Suggestions";
import WrongPassword from "./components/script/WrongPassword";
import AccessGranted from "./components/script/AccessGranted";

export const guestHost = () => {
    return {
        type: outputLineTypes.guestHost,
        props: {
            text: ""
        },
        component: GuestHost
    }
}
export const enterPassword = () => {
    return {
        type: outputLineTypes.enterPassword,
        component: EnterPassword
    }
}

export const input = (text) => {
    return {
        type: outputLineTypes.input,
        props: {
            text: typeof text === 'function' ? text() : text
        },
        component: Input
    }
}

export const executable = (command, state) => {
    return {
        type: outputLineTypes.executable,
        props: {
            state,
            command,
            text: ""
        },
        component: Script, YesScript
    }
}

export const suggestions = (input) => {
    return {
        type: outputLineTypes.suggestions,
        input,
        suggestions: getSuggestions(input),
        component: Suggestions
    }
}

export const noCommandFound = (command) => {
    return {
        type: outputLineTypes.noCommandFound,
        props: {
            command,
            text: command + ": " + messages.noCommandFound
        },
        component: NoCommandFound
    };
}

export const wrongPassword = () => {
    return {
        type: outputLineTypes.wrongPassword,
        props: {
            text: messages.wrongPassword
        },
        component: WrongPassword
    };
}

export const accessGranted = () => {
    return {
        type: outputLineTypes.accessGranted,
        props: {
            text: messages.accessGranted
        },
        component: AccessGranted
    };
}

// Function that adds new GuestHost object to the stack
export function addGuestHost(stack){
    stack.push(guestHost());
    return stack;
}

export function addEnterPassword (stack) {
    stack.push(enterPassword());
    return stack;
}

// Function that adds new input to the stack
export function addInput(stack, text = ""){
    stack.push(input(text));
    return stack;
}

// Function to add executable to the stack
export function addExecutable(command, stack, props){
    stack.push(executable(command, props));
    return stack;
}

// Function to add executable to the stack
export function addSuggestions(input, stack){ 
    stack.push(suggestions(input));
    return stack;
}

// Function to no command found input type to the stack
export function addNoCommandFound(stack, command){ 
    stack.push(noCommandFound(command));
    return stack;
}

export function addWrongPassword(stack){
    stack.push(wrongPassword());
    return stack;
}

export function addAccessGranted(stack){
    stack.push(accessGranted());
    return stack;
}

// Function that concats input to the already typed in text
export function concatInput(input, stack){
    // Get the last input type element in the stack,
    // concat new input to it input
    stack.reverse();
    for(let i = 0; i < stack.length; i++) {
        if(stack[i].type === outputLineTypes.input) {
            stack[i].props.text = stack[i].props.text.concat(input);
            break;
        }
    }
    stack.reverse();
    return stack;
}

// Check if an input command is valid
export function inputIsValid(input) {
    let command;
    // From the commands array, see if any command alias matched with input
    Object.entries(commands).map(([key,element]) => {
        // aliases
        if(element.aliases.includes(input)) {
            command = key;
        }
    });
    return typeof command === "undefined" ? false : command;
}

// Check if the input command is "clear"
// "c" is an alias for "clear"
export function isClear(input) {
    return input === commands['clear'].name || input === "c";
}

export function isPassword (input) {
    return passwordRequiredCommands.some(cmd => commands[cmd].aliases.includes(input));
}

export function getLoginAction(commandKey) {
    return commands[commandKey].loginAction;
}

export function getCommandName(commandKey) {
  return commands[commandKey].name;
}

// Returns an array with different suggestions

export function getSuggestions(input) {
    let suggestions = [];
    Object.entries(commands).filter(([key,cmd]) => cmd.hidden === undefined || !cmd.hidden)
      .map(([key,cmd], i) => {
        cmd.aliases.forEach((alias, j) => {
            if(alias.substr(0, input.length) === input){
                suggestions.push(alias);
            }
        })
    });
    return suggestions;
}

/* 
    Constant variables used by App
*/
export const initialOutput = () => [
    executable("reset"),
    guestHost(),
    input(messages.welcome),
    guestHost(),
    input("")
];

export const reinitializedOutput = [
    guestHost(),
    input("")
];